﻿/*
 * Created by SharpDevelop.
 * User: sumit_shrestha1
 * Date: 6/14/2019
 * Time: 1:08 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace all_steps
{
    partial class Form2
    {
        /// <summary>
        /// Designer variable used to keep track of non-visual components.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Disposes resources used by the form.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing)
        {
            if (components != null)
            {
                components.Dispose();
            }
        }
        base.Dispose(disposing);
    }

    /// <summary>
    /// This method is required for Windows Forms designer support.
    /// Do not change the method contents inside the source code editor. The Forms designer might
    /// not be able to load this method if it was changed manually.
    /// </summary>
    private void InitializeComponent()
    {
    	this.comboBox1 = new System.Windows.Forms.ComboBox();
    	this.button1 = new System.Windows.Forms.Button();
    	this.button2 = new System.Windows.Forms.Button();
    	this.comboBox2 = new System.Windows.Forms.ComboBox();
    	this.listBox1 = new System.Windows.Forms.ListBox();
    	this.button3 = new System.Windows.Forms.Button();
    	this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
    	this.label16 = new System.Windows.Forms.Label();
    	this.label15 = new System.Windows.Forms.Label();
    	this.label14 = new System.Windows.Forms.Label();
    	this.label17 = new System.Windows.Forms.Label();
    	this.panel1 = new System.Windows.Forms.Panel();
    	this.label18 = new System.Windows.Forms.Label();
    	this.button5 = new System.Windows.Forms.Button();
    	this.label13 = new System.Windows.Forms.Label();
    	this.button4 = new System.Windows.Forms.Button();
    	this.panel3 = new System.Windows.Forms.Panel();
    	this.textBox4 = new System.Windows.Forms.TextBox();
    	this.label12 = new System.Windows.Forms.Label();
    	this.textBox1 = new System.Windows.Forms.TextBox();
    	this.comboBox5 = new System.Windows.Forms.ComboBox();
    	this.label11 = new System.Windows.Forms.Label();
    	this.textBox3 = new System.Windows.Forms.TextBox();
    	this.label10 = new System.Windows.Forms.Label();
    	this.label9 = new System.Windows.Forms.Label();
    	this.textBox2 = new System.Windows.Forms.TextBox();
    	this.label8 = new System.Windows.Forms.Label();
    	this.comboBox4 = new System.Windows.Forms.ComboBox();
    	this.label7 = new System.Windows.Forms.Label();
    	this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
    	this.label6 = new System.Windows.Forms.Label();
    	this.comboBox3 = new System.Windows.Forms.ComboBox();
    	this.label5 = new System.Windows.Forms.Label();
    	this.radioButton2 = new System.Windows.Forms.RadioButton();
    	this.radioButton1 = new System.Windows.Forms.RadioButton();
    	this.label4 = new System.Windows.Forms.Label();
    	this.label3 = new System.Windows.Forms.Label();
    	this.label2 = new System.Windows.Forms.Label();
    	this.label1 = new System.Windows.Forms.Label();
    	this.panel2 = new System.Windows.Forms.Panel();
    	this.panel4 = new System.Windows.Forms.Panel();
    	this.panel1.SuspendLayout();
    	this.panel3.SuspendLayout();
    	this.panel4.SuspendLayout();
    	this.SuspendLayout();
    	// 
    	// comboBox1
    	// 
    	this.comboBox1.FormattingEnabled = true;
    	this.comboBox1.Location = new System.Drawing.Point(7, 33);
    	this.comboBox1.Name = "comboBox1";
    	this.comboBox1.Size = new System.Drawing.Size(258, 23);
    	this.comboBox1.TabIndex = 0;
    	this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.ComboBox1SelectedIndexChanged);
    	// 
    	// button1
    	// 
    	this.button1.Location = new System.Drawing.Point(9, 67);
    	this.button1.Name = "button1";
    	this.button1.Size = new System.Drawing.Size(112, 27);
    	this.button1.TabIndex = 1;
    	this.button1.Text = "Select Ontology";
    	this.button1.UseCompatibleTextRendering = true;
    	this.button1.UseVisualStyleBackColor = true;
    	this.button1.Click += new System.EventHandler(this.Button1Click);
    	// 
    	// button2
    	// 
    	this.button2.Location = new System.Drawing.Point(9, 151);
    	this.button2.Name = "button2";
    	this.button2.Size = new System.Drawing.Size(87, 27);
    	this.button2.TabIndex = 2;
    	this.button2.Text = "Select Node";
    	this.button2.UseCompatibleTextRendering = true;
    	this.button2.UseVisualStyleBackColor = true;
    	this.button2.Click += new System.EventHandler(this.Button2Click);
    	// 
    	// comboBox2
    	// 
    	this.comboBox2.FormattingEnabled = true;
    	this.comboBox2.Location = new System.Drawing.Point(9, 122);
    	this.comboBox2.Name = "comboBox2";
    	this.comboBox2.Size = new System.Drawing.Size(416, 23);
    	this.comboBox2.TabIndex = 3;
    	// 
    	// listBox1
    	// 
    	this.listBox1.FormattingEnabled = true;
    	this.listBox1.ItemHeight = 15;
    	this.listBox1.Location = new System.Drawing.Point(461, 49);
    	this.listBox1.Name = "listBox1";
    	this.listBox1.Size = new System.Drawing.Size(139, 109);
    	this.listBox1.TabIndex = 8;
    	// 
    	// button3
    	// 
    	this.button3.Location = new System.Drawing.Point(606, 49);
    	this.button3.Name = "button3";
    	this.button3.Size = new System.Drawing.Size(87, 27);
    	this.button3.TabIndex = 9;
    	this.button3.Text = "Go Back";
    	this.button3.UseCompatibleTextRendering = true;
    	this.button3.UseVisualStyleBackColor = true;
    	this.button3.Click += new System.EventHandler(this.Button3Click);
    	// 
    	// tableLayoutPanel1
    	// 
    	this.tableLayoutPanel1.AllowDrop = true;
    	this.tableLayoutPanel1.AutoSize = true;
    	this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
    	this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
    	this.tableLayoutPanel1.ColumnCount = 4;
    	this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
    	this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
    	this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
    	this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
    	this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
    	this.tableLayoutPanel1.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
    	this.tableLayoutPanel1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
    	this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
    	this.tableLayoutPanel1.Name = "tableLayoutPanel1";
    	this.tableLayoutPanel1.RowCount = 1;
    	this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
    	this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
    	this.tableLayoutPanel1.Size = new System.Drawing.Size(507, 31);
    	this.tableLayoutPanel1.TabIndex = 5;
    	// 
    	// label16
    	// 
    	this.label16.Anchor = System.Windows.Forms.AnchorStyles.None;
    	this.label16.AutoSize = true;
    	this.label16.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
    	this.label16.Location = new System.Drawing.Point(765, 1);
    	this.label16.Name = "label16";
    	this.label16.Size = new System.Drawing.Size(120, 29);
    	this.label16.TabIndex = 3;
    	this.label16.Text = "Node:Entropy";
    	this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
    	this.label16.UseCompatibleTextRendering = true;
    	// 
    	// label15
    	// 
    	this.label15.Anchor = System.Windows.Forms.AnchorStyles.None;
    	this.label15.AutoSize = true;
    	this.label15.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
    	this.label15.Location = new System.Drawing.Point(484, 1);
    	this.label15.Name = "label15";
    	this.label15.Size = new System.Drawing.Size(99, 29);
    	this.label15.TabIndex = 2;
    	this.label15.Text = "Node:Label";
    	this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
    	this.label15.UseCompatibleTextRendering = true;
    	// 
    	// label14
    	// 
    	this.label14.Anchor = System.Windows.Forms.AnchorStyles.None;
    	this.label14.AutoSize = true;
    	this.label14.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
    	this.label14.Location = new System.Drawing.Point(175, 1);
    	this.label14.Name = "label14";
    	this.label14.Size = new System.Drawing.Size(135, 29);
    	this.label14.TabIndex = 1;
    	this.label14.Text = "Ontology: Label";
    	this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
    	this.label14.UseCompatibleTextRendering = true;
    	// 
    	// label17
    	// 
    	this.label17.Location = new System.Drawing.Point(0, 0);
    	this.label17.Name = "label17";
    	this.label17.Size = new System.Drawing.Size(100, 23);
    	this.label17.TabIndex = 0;
    	this.label17.UseCompatibleTextRendering = true;
    	// 
    	// panel1
    	// 
    	this.panel1.Controls.Add(this.label18);
    	this.panel1.Controls.Add(this.button5);
    	this.panel1.Controls.Add(this.label13);
    	this.panel1.Controls.Add(this.button4);
    	this.panel1.Controls.Add(this.panel3);
    	this.panel1.Controls.Add(this.label3);
    	this.panel1.Controls.Add(this.label2);
    	this.panel1.Controls.Add(this.comboBox1);
    	this.panel1.Controls.Add(this.button1);
    	this.panel1.Controls.Add(this.button3);
    	this.panel1.Controls.Add(this.comboBox2);
    	this.panel1.Controls.Add(this.listBox1);
    	this.panel1.Controls.Add(this.button2);
    	this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
    	this.panel1.Location = new System.Drawing.Point(0, 0);
    	this.panel1.Name = "panel1";
    	this.panel1.Size = new System.Drawing.Size(738, 772);
    	this.panel1.TabIndex = 11;
    	// 
    	// label18
    	// 
    	this.label18.Location = new System.Drawing.Point(24, 716);
    	this.label18.Name = "label18";
    	this.label18.Size = new System.Drawing.Size(164, 23);
    	this.label18.TabIndex = 16;
    	this.label18.Text = "Click to reset all input fields";
    	this.label18.UseCompatibleTextRendering = true;
    	// 
    	// button5
    	// 
    	this.button5.Location = new System.Drawing.Point(194, 710);
    	this.button5.Name = "button5";
    	this.button5.Size = new System.Drawing.Size(105, 27);
    	this.button5.TabIndex = 15;
    	this.button5.Text = "Reset Form";
    	this.button5.UseCompatibleTextRendering = true;
    	this.button5.UseVisualStyleBackColor = true;
    	this.button5.Click += new System.EventHandler(this.Button5Click);
    	// 
    	// label13
    	// 
    	this.label13.Location = new System.Drawing.Point(461, 20);
    	this.label13.Name = "label13";
    	this.label13.Size = new System.Drawing.Size(100, 23);
    	this.label13.TabIndex = 14;
    	this.label13.Text = "Upward Closure";
    	this.label13.UseCompatibleTextRendering = true;
    	// 
    	// button4
    	// 
    	this.button4.Location = new System.Drawing.Point(13, 658);
    	this.button4.Name = "button4";
    	this.button4.Size = new System.Drawing.Size(75, 23);
    	this.button4.TabIndex = 13;
    	this.button4.Text = "Submit";
    	this.button4.UseCompatibleTextRendering = true;
    	this.button4.UseVisualStyleBackColor = true;
    	this.button4.Click += new System.EventHandler(this.Button4Click);
    	// 
    	// panel3
    	// 
    	this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
    	this.panel3.Controls.Add(this.textBox4);
    	this.panel3.Controls.Add(this.label12);
    	this.panel3.Controls.Add(this.textBox1);
    	this.panel3.Controls.Add(this.comboBox5);
    	this.panel3.Controls.Add(this.label11);
    	this.panel3.Controls.Add(this.textBox3);
    	this.panel3.Controls.Add(this.label10);
    	this.panel3.Controls.Add(this.label9);
    	this.panel3.Controls.Add(this.textBox2);
    	this.panel3.Controls.Add(this.label8);
    	this.panel3.Controls.Add(this.comboBox4);
    	this.panel3.Controls.Add(this.label7);
    	this.panel3.Controls.Add(this.checkedListBox1);
    	this.panel3.Controls.Add(this.label6);
    	this.panel3.Controls.Add(this.comboBox3);
    	this.panel3.Controls.Add(this.label5);
    	this.panel3.Controls.Add(this.radioButton2);
    	this.panel3.Controls.Add(this.radioButton1);
    	this.panel3.Controls.Add(this.label4);
    	this.panel3.Location = new System.Drawing.Point(8, 184);
    	this.panel3.Name = "panel3";
    	this.panel3.Size = new System.Drawing.Size(725, 457);
    	this.panel3.TabIndex = 12;
    	this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel3Paint);
    	// 
    	// textBox4
    	// 
    	this.textBox4.Location = new System.Drawing.Point(433, 359);
    	this.textBox4.Multiline = true;
    	this.textBox4.Name = "textBox4";
    	this.textBox4.ReadOnly = true;
    	this.textBox4.Size = new System.Drawing.Size(281, 61);
    	this.textBox4.TabIndex = 18;
    	this.textBox4.TextChanged += new System.EventHandler(this.TextBox4TextChanged);
    	// 
    	// label12
    	// 
    	this.label12.Location = new System.Drawing.Point(295, 362);
    	this.label12.Name = "label12";
    	this.label12.Size = new System.Drawing.Size(143, 63);
    	this.label12.TabIndex = 17;
    	this.label12.Text = "What is the life safety width requirement for stairs?";
    	this.label12.UseCompatibleTextRendering = true;
    	// 
    	// textBox1
    	// 
    	this.textBox1.Location = new System.Drawing.Point(296, 221);
    	this.textBox1.Multiline = true;
    	this.textBox1.Name = "textBox1";
    	this.textBox1.Size = new System.Drawing.Size(353, 65);
    	this.textBox1.TabIndex = 10;
    	this.textBox1.TextChanged += new System.EventHandler(this.TextBox1TextChanged);
    	// 
    	// comboBox5
    	// 
    	this.comboBox5.Enabled = false;
    	this.comboBox5.FormattingEnabled = true;
    	this.comboBox5.Location = new System.Drawing.Point(174, 359);
    	this.comboBox5.Name = "comboBox5";
    	this.comboBox5.Size = new System.Drawing.Size(83, 23);
    	this.comboBox5.TabIndex = 16;
    	this.comboBox5.SelectedIndexChanged += new System.EventHandler(this.ComboBox5SelectedIndexChanged);
    	// 
    	// label11
    	// 
    	this.label11.Location = new System.Drawing.Point(15, 359);
    	this.label11.Name = "label11";
    	this.label11.Size = new System.Drawing.Size(143, 63);
    	this.label11.TabIndex = 15;
    	this.label11.Text = "What are the minimum number of exits or access to exits per story?";
    	this.label11.UseCompatibleTextRendering = true;
    	// 
    	// textBox3
    	// 
    	this.textBox3.Location = new System.Drawing.Point(295, 292);
    	this.textBox3.Multiline = true;
    	this.textBox3.Name = "textBox3";
    	this.textBox3.ReadOnly = true;
    	this.textBox3.Size = new System.Drawing.Size(353, 61);
    	this.textBox3.TabIndex = 14;
    	this.textBox3.TextChanged += new System.EventHandler(this.TextBox3TextChanged);
    	// 
    	// label10
    	// 
    	this.label10.Location = new System.Drawing.Point(15, 294);
    	this.label10.Name = "label10";
    	this.label10.Size = new System.Drawing.Size(275, 33);
    	this.label10.TabIndex = 13;
    	this.label10.Text = "What is the means of egress sizing for stairways?";
    	this.label10.UseCompatibleTextRendering = true;
    	// 
    	// label9
    	// 
    	this.label9.Location = new System.Drawing.Point(13, 157);
    	this.label9.Name = "label9";
    	this.label9.Size = new System.Drawing.Size(275, 23);
    	this.label9.TabIndex = 12;
    	this.label9.Text = "What is the occupant load?";
    	this.label9.UseCompatibleTextRendering = true;
    	// 
    	// textBox2
    	// 
    	this.textBox2.Location = new System.Drawing.Point(295, 154);
    	this.textBox2.Multiline = true;
    	this.textBox2.Name = "textBox2";
    	this.textBox2.Size = new System.Drawing.Size(353, 61);
    	this.textBox2.TabIndex = 11;
    	this.textBox2.TextChanged += new System.EventHandler(this.TextBox2TextChanged);
    	// 
    	// label8
    	// 
    	this.label8.Location = new System.Drawing.Point(15, 224);
    	this.label8.Name = "label8";
    	this.label8.Size = new System.Drawing.Size(275, 23);
    	this.label8.TabIndex = 9;
    	this.label8.Text = "What is your building construction type?";
    	this.label8.UseCompatibleTextRendering = true;
    	// 
    	// comboBox4
    	// 
    	this.comboBox4.FormattingEnabled = true;
    	this.comboBox4.Location = new System.Drawing.Point(638, 54);
    	this.comboBox4.Name = "comboBox4";
    	this.comboBox4.Size = new System.Drawing.Size(67, 23);
    	this.comboBox4.TabIndex = 8;
    	this.comboBox4.SelectedIndexChanged += new System.EventHandler(this.ComboBox4SelectedIndexChanged);
    	// 
    	// label7
    	// 
    	this.label7.Location = new System.Drawing.Point(421, 56);
    	this.label7.Name = "label7";
    	this.label7.Size = new System.Drawing.Size(275, 23);
    	this.label7.TabIndex = 7;
    	this.label7.Text = "What is the building occupancy type?";
    	this.label7.UseCompatibleTextRendering = true;
    	// 
    	// checkedListBox1
    	// 
    	this.checkedListBox1.FormattingEnabled = true;
    	this.checkedListBox1.Location = new System.Drawing.Point(295, 54);
    	this.checkedListBox1.Name = "checkedListBox1";
    	this.checkedListBox1.Size = new System.Drawing.Size(120, 94);
    	this.checkedListBox1.TabIndex = 6;
    	this.checkedListBox1.UseCompatibleTextRendering = true;
    	this.checkedListBox1.SelectedIndexChanged += new System.EventHandler(this.CheckedListBox1SelectedIndexChanged);
    	// 
    	// label6
    	// 
    	this.label6.Location = new System.Drawing.Point(15, 56);
    	this.label6.Name = "label6";
    	this.label6.Size = new System.Drawing.Size(275, 37);
    	this.label6.TabIndex = 5;
    	this.label6.Text = "What known weather risks are prevalent at building location?";
    	this.label6.UseCompatibleTextRendering = true;
    	// 
    	// comboBox3
    	// 
    	this.comboBox3.FormattingEnabled = true;
    	this.comboBox3.Location = new System.Drawing.Point(593, 20);
    	this.comboBox3.Name = "comboBox3";
    	this.comboBox3.Size = new System.Drawing.Size(121, 23);
    	this.comboBox3.TabIndex = 4;
    	this.comboBox3.SelectedIndexChanged += new System.EventHandler(this.ComboBox3SelectedIndexChanged);
    	// 
    	// label5
    	// 
    	this.label5.Location = new System.Drawing.Point(372, 17);
    	this.label5.Name = "label5";
    	this.label5.Size = new System.Drawing.Size(233, 33);
    	this.label5.TabIndex = 3;
    	this.label5.Text = "Which state or province is the building located?";
    	this.label5.UseCompatibleTextRendering = true;
    	// 
    	// radioButton2
    	// 
    	this.radioButton2.Location = new System.Drawing.Point(311, 17);
    	this.radioButton2.Name = "radioButton2";
    	this.radioButton2.Size = new System.Drawing.Size(46, 24);
    	this.radioButton2.TabIndex = 2;
    	this.radioButton2.TabStop = true;
    	this.radioButton2.Text = "No";
    	this.radioButton2.UseCompatibleTextRendering = true;
    	this.radioButton2.UseVisualStyleBackColor = true;
    	this.radioButton2.CheckedChanged += new System.EventHandler(this.RadioButton2CheckedChanged);
    	// 
    	// radioButton1
    	// 
    	this.radioButton1.Location = new System.Drawing.Point(266, 17);
    	this.radioButton1.Name = "radioButton1";
    	this.radioButton1.Size = new System.Drawing.Size(56, 24);
    	this.radioButton1.TabIndex = 1;
    	this.radioButton1.TabStop = true;
    	this.radioButton1.Text = "Yes";
    	this.radioButton1.UseCompatibleTextRendering = true;
    	this.radioButton1.UseVisualStyleBackColor = true;
    	this.radioButton1.CheckedChanged += new System.EventHandler(this.RadioButton1CheckedChanged);
    	// 
    	// label4
    	// 
    	this.label4.Location = new System.Drawing.Point(13, 19);
    	this.label4.Name = "label4";
    	this.label4.Size = new System.Drawing.Size(260, 23);
    	this.label4.TabIndex = 0;
    	this.label4.Text = "Does the building have more than one story?";
    	this.label4.UseCompatibleTextRendering = true;
    	this.label4.Click += new System.EventHandler(this.Label4Click);
    	// 
    	// label3
    	// 
    	this.label3.AutoSize = true;
    	this.label3.Location = new System.Drawing.Point(12, 101);
    	this.label3.Name = "label3";
    	this.label3.Size = new System.Drawing.Size(36, 21);
    	this.label3.TabIndex = 11;
    	this.label3.Text = "Node";
    	this.label3.UseCompatibleTextRendering = true;
    	// 
    	// label2
    	// 
    	this.label2.AutoSize = true;
    	this.label2.Location = new System.Drawing.Point(7, 9);
    	this.label2.Name = "label2";
    	this.label2.Size = new System.Drawing.Size(58, 21);
    	this.label2.TabIndex = 10;
    	this.label2.Text = "Ontology";
    	this.label2.UseCompatibleTextRendering = true;
    	this.label2.Click += new System.EventHandler(this.Label2_Click);
    	// 
    	// label1
    	// 
    	this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
    	this.label1.AutoSize = true;
    	this.label1.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
    	this.label1.Location = new System.Drawing.Point(40, 1);
    	this.label1.Name = "label1";
    	this.label1.Size = new System.Drawing.Size(17, 29);
    	this.label1.TabIndex = 4;
    	this.label1.Text = "#";
    	this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
    	this.label1.UseCompatibleTextRendering = true;
    	this.label1.Click += new System.EventHandler(this.Label1_Click);
    	// 
    	// panel2
    	// 
    	this.panel2.AutoScroll = true;
    	this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
    	this.panel2.Location = new System.Drawing.Point(5, 5);
    	this.panel2.Margin = new System.Windows.Forms.Padding(25, 0, 25, 25);
    	this.panel2.Name = "panel2";
    	this.panel2.Size = new System.Drawing.Size(653, 760);
    	this.panel2.TabIndex = 12;
    	this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel2Paint);
    	// 
    	// panel4
    	// 
    	this.panel4.AutoScroll = true;
    	this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
    	this.panel4.Controls.Add(this.panel2);
    	this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
    	this.panel4.Location = new System.Drawing.Point(738, 0);
    	this.panel4.Margin = new System.Windows.Forms.Padding(0);
    	this.panel4.Name = "panel4";
    	this.panel4.Padding = new System.Windows.Forms.Padding(5);
    	this.panel4.Size = new System.Drawing.Size(665, 772);
    	this.panel4.TabIndex = 13;
    	// 
    	// Form2
    	// 
    	this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
    	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
    	this.AutoScroll = true;
    	this.AutoScrollMargin = new System.Drawing.Size(5, 5);
    	this.AutoSize = true;
    	this.BackColor = System.Drawing.SystemColors.ButtonFace;
    	this.ClientSize = new System.Drawing.Size(1403, 772);
    	this.Controls.Add(this.panel4);
    	this.Controls.Add(this.panel1);
    	this.Font = new System.Drawing.Font("Candara", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
    	this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
    	this.Name = "Form2";
    	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
    	this.Text = "CrossWalk";
    	this.Load += new System.EventHandler(this.Form2Load);
    	this.panel1.ResumeLayout(false);
    	this.panel1.PerformLayout();
    	this.panel3.ResumeLayout(false);
    	this.panel3.PerformLayout();
    	this.panel4.ResumeLayout(false);
    	this.ResumeLayout(false);
    }
    private System.Windows.Forms.Panel panel4;
    private System.Windows.Forms.Label label18;
    private System.Windows.Forms.Button button5;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.TextBox textBox3;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.ComboBox comboBox5;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.TextBox textBox4;
    private System.Windows.Forms.Button button4;
    private System.Windows.Forms.RadioButton radioButton1;
    private System.Windows.Forms.RadioButton radioButton2;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.ComboBox comboBox3;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.CheckedListBox checkedListBox1;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.ComboBox comboBox4;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.TextBox textBox1;
    private System.Windows.Forms.TextBox textBox2;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Panel panel3;
    private System.Windows.Forms.Button button3;
    private System.Windows.Forms.ListBox listBox1;
    private System.Windows.Forms.ComboBox comboBox2;
    private System.Windows.Forms.Button button2;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.ComboBox comboBox1;
    private System.Windows.Forms.Label label16;
    private System.Windows.Forms.Label label17;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
   

        void Label14Click(object sender, System.EventArgs e)
    {

    }

       // private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    
    
    void Button5Click(object sender, System.EventArgs e)
    {
    	this.Controls.Clear();
    	this.InitializeComponent();
    	this.FillAllValues();
    }
    }
}