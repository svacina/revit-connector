﻿/*
 * Created by SharpDevelop.
 * User: sumit_shrestha1
 * Date: 6/14/2019
 * Time: 1:08 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.IO;
using Autodesk.Revit.UI;
using System.Web.Script.Serialization;

namespace Revit_Plugin
{
    /// <summary>
    /// Description of Form2.
    /// </summary>
    public partial class Form2 : Form
    {

        public String selectedItem = "";
        List<Node> nodeList = new List<Form2.Node> { };
        List<Ontology> ontologyList = new List<Form2.Ontology> { };
        List<Metadata> metadataList = new List<Form2.Metadata> { };
        List<Node> upwardClosure = new List<Node> { };
        List<TreeElement> treeElements = new List<Form2.TreeElement> { };
        Ontology selectedOntology = new Form2.Ontology { };
        String[] answerList = new String[9];


        List<String> weatherRisks = new List<String> { "Hurricanes", "Fires", "Earthquakes", "Tornadoes", "Snow Storms", "Flooding", "Drought", "Others" };
        List<String> stories = new List<String> { "1", "2", "3", "4", "More than 4" };

        double occupantLoadMultiplier = 0.3;


        public Form2()
        {
            //
            // The InitializeComponent() call is required for Windows Forms designer support.
            //

            InitializeComponent();
            FillAllValues();


            //
            // TODO: Add constructor code after the InitializeComponent() call.
            //

        }

        public void FillAllValues()
        {
            String url = String.Format("https://us-central1-sbir-80f98.cloudfunctions.net/revit/getOntologies");
            String payload = "{\"password\":\"uj287h7dnd342ijf\"}";
            String response = callClientAPI(url, payload);


            var serializer = new JavaScriptSerializer();
            var deserializedResult = serializer.Deserialize<List<Ontology>>(response);

            ontologyList = deserializedResult.ToList();


            foreach (Ontology ont in ontologyList)
            {
                this.comboBox1.Items.Add(ont.label);

            }


            this.comboBox2.Visible = false;
            this.button2.Visible = false;
            this.tableLayoutPanel1.Visible = false;
            // this.tableLayoutPanel2.Visible = false;


            foreach (String weatherRisk in weatherRisks)
            {
                this.checkedListBox1.Items.Add(weatherRisk);

            }



            foreach (String story in stories)
            {
                this.comboBox5.Items.Add(story);

            }

            this.comboBox3.DataSource = LoadStatesToComboBox();
            this.comboBox4.DataSource = LoadBuildingOccupancy();

        }

        private List<String> LoadBuildingOccupancy()
        {
            List<String> occupancy = new List<String>();
            occupancy.Add("A-1");
            occupancy.Add("A-2");
            occupancy.Add("A-3");
            occupancy.Add("A-4");
            occupancy.Add("A-5");
            occupancy.Add("B");
            occupancy.Add("E");
            occupancy.Add("F-1");
            occupancy.Add("F-2");
            occupancy.Add("H-1");
            occupancy.Add("H-2");
            occupancy.Add("H-3");
            occupancy.Add("H-4");
            occupancy.Add("H-5");
            occupancy.Add("I-1");
            occupancy.Add("I-2");
            occupancy.Add("I-3");
            occupancy.Add("I-4");
            occupancy.Add("M");
            occupancy.Add("R-1");
            occupancy.Add("R-2");
            occupancy.Add("R-3");
            occupancy.Add("R-4");
            occupancy.Add("S-1");
            occupancy.Add("S-2");
            occupancy.Add("U");
            return occupancy;
        }

        private List<String> LoadStatesToComboBox()
        {
            List<String> states = new List<String>();

            states.Add("Alabama");
            states.Add("Alaska");
            states.Add("Arizona");
            states.Add("Arkansas");
            states.Add("California");
            states.Add("Colorado");
            states.Add("Connecticut");
            states.Add("Delaware");
            states.Add("District Of Columbia");
            states.Add("Florida");
            states.Add("Georgia");
            states.Add("Hawaii");
            states.Add("Idaho");
            states.Add("Illinois");
            states.Add("Indiana");
            states.Add("Iowa");
            states.Add("Kansas");
            states.Add("Kentucky");
            states.Add("Louisiana");
            states.Add("Maine");
            states.Add("Maryland");
            states.Add("Massachusetts");
            states.Add("Michigan");
            states.Add("Minnesota");
            states.Add("Mississippi");
            states.Add("Missouri");
            states.Add("Montana");
            states.Add("Nebraska");
            states.Add("Nevada");
            states.Add("New Hampshire");
            states.Add("New Jersey");
            states.Add("New Mexico");
            states.Add("New York");
            states.Add("North Carolina");
            states.Add("North Dakota");
            states.Add("Ohio");
            states.Add("Oklahoma");
            states.Add("Oregon");
            states.Add("Pennsylvania");
            states.Add("Rhode Island");
            states.Add("South Carolina");
            states.Add("South Dakota");
            states.Add("Tennessee");
            states.Add("Texas");
            states.Add("Utah");
            states.Add("Vermont");
            states.Add("Virginia");
            states.Add("Washington");
            states.Add("West Virginia");
            states.Add("Wisconsin");
            states.Add("Wyoming");
            return states;
        }


        void ComboBox1SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        // this function fetches handles click event for ontology
        void Button1Click(object sender, EventArgs e)
        {
            var txt = this.comboBox1.Text;
            this.comboBox1.Enabled = false;
            this.button1.Visible = false;

            this.comboBox2.Visible = true;
            this.button2.Visible = true;

            selectedOntology = getOntologyBylabel(txt);


            //now call API getRoot and fetch a list
            String url = String.Format("https://us-central1-sbir-80f98.cloudfunctions.net/revit/getRoot");
            String payload = "{\"password\":\"uj287h7dnd342ijf\", \"ontologyId\":\"" + selectedOntology.id + "\"}";

            String response = callClientAPI(url, payload);
            var serializer = new JavaScriptSerializer();
            var deserializedResult = serializer.Deserialize<Node>(response);
            Node rootNode = deserializedResult;

            //fetch tree
            url = String.Format("https://us-central1-sbir-80f98.cloudfunctions.net/revit/getTree");
            payload = "{\"password\":\"uj287h7dnd342ijf\", \"ontologyId\":\"" + selectedOntology.id + "\"}";
            response = callClientAPI(url, payload);
            var serializerTree = new JavaScriptSerializer();
            var deserializedTreeResult = serializerTree.Deserialize<List<TreeElement>>(response);
            treeElements = deserializedTreeResult.ToList();

            upwardClosure.Add(rootNode);
            this.listBox1.Items.Add(rootNode.label);
            this.label3.Text = "Select children node of " + rootNode.label;

            List<Node> childrenNodes = fetchChildrenNode(rootNode);
            foreach (Node node in childrenNodes)
            {
                this.comboBox2.Items.Add(node.label);

            }

            //MessageBox.Show(txt);  
        }
        private Label getLabelFromText(string text)
        {
            Label temp = new Label();
            temp.Anchor = System.Windows.Forms.AnchorStyles.None;
            temp.AutoSize = true;
            temp.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            temp.Location = new System.Drawing.Point(17, 3);
            temp.Name = "label13";
            temp.Size = new System.Drawing.Size(17, 29);
            temp.TabIndex = 0;
            temp.Text = text;
            temp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            temp.UseCompatibleTextRendering = true;
            return temp;
        }

        void Button2Click(object sender, EventArgs e)
        {
            var txt = this.comboBox2.Text; //this fetches the selected item from drodown. need a different way to fetch current object rather than display text.
            Node selectedNode = getNodeByLabel(txt);
            List<Node> childrenNodes = fetchChildrenNode(selectedNode);

            /*
			 * if children Nodes are empty that means it is the end then call Metadata api
			 * else populate comboBox2 with children nodes.
			 * 
			*/

            upwardClosure.Add(selectedNode);
            this.listBox1.Items.Add(selectedNode.label);
            if (upwardClosure.Count > 1)
            {
                this.button3.Visible = true;
            }

            if (childrenNodes.Count == 0)
            {


                /*
                this.panel1.Visible = true;
                this.textBox1.Text = metadataList[0].id;
                this.textBox2.Text = metadataList[0].reviewer;
                this.textBox3.Text = metadataList[0].contentProvider;
                this.textBox4.Text = metadataList[0].review.title;
                this.textBox5.Text = metadataList[0].review.limitations;
                this.textBox6.Text = metadataList[0].review.objectives;
                this.textBox7.Text = metadataList[0].review.sampling;
                this.textBox8.Text = metadataList[0].review.levelDescription;			
                this.textBox9.Text = metadataList[0].review.dataCollection1;
                this.textBox10.Text = metadataList[0].review.variables;
                */

            }

            else
            {
                this.label3.Text = "Select children node of " + selectedNode.label;
                this.comboBox2.SelectedIndex = -1;
                this.comboBox2.ResetText();
                this.comboBox2.Items.Clear();
                foreach (Node node in childrenNodes)
                {
                    this.comboBox2.Items.Add(node.label);
                }


            }





            //TaskDialog.Show("whoa",response);
        }

        String callClientAPI(String url, String payload)
        {

            string stringurl = url;
            WebRequest requestObj = WebRequest.Create(stringurl);
            requestObj.Method = "POST";
            requestObj.ContentType = "application/json";


            string postData = payload;
            //HttpWebResponse responseObject = null;
            //responseObject = (HttpWebResponse)requestObj.GetResponse();


            using (var streamWriter = new StreamWriter(requestObj.GetRequestStream()))
            {
                streamWriter.Write(postData);
                streamWriter.Flush();
                streamWriter.Close();

                var httpResponse = (HttpWebResponse)requestObj.GetResponse();


                string resultstring = null;
                using (var dataStream = new StreamReader(httpResponse.GetResponseStream()))
                {

                    var result2 = dataStream.ReadToEnd();
                    resultstring = result2;

                }
                return resultstring;

            }

        }

        Ontology getOntologyBylabel(string label)
        {
            foreach (Ontology ont in ontologyList)
            {
                if (ont.label == label)
                    return ont;
            }
            return null;

        }

        Node getNodeByLabel(string label)
        {
            foreach (TreeElement elem in treeElements)
            {
                if (elem.node.label == label)
                    return elem.node;
            }
            return null;

        }

        Node getNodeById(string id)
        {
            foreach (TreeElement elem in treeElements)
            {
                if (elem.node.id == id)
                    return elem.node;
            }
            return null;

        }

        List<Node> fetchChildrenNode(Node node)
        {

            List<Node> childrenNode = new List<Form2.Node> { };
            List<String> childrenIds = new List<string> { };

            foreach (TreeElement elem in this.treeElements)
            {
                if (elem.node.id == node.id)
                {
                    childrenIds = elem.children;
                }
            }

            foreach (String nodeId in childrenIds)
            {
                childrenNode.Add(getNodeById(nodeId));

            }
            return childrenNode;


        }



        public class Node
        {

            public string id;
            public string label;
            public float entropy;
        }

        public class TreeElement
        {
            public Node node;
            public List<String> children;
        }

        public class Ontology
        {

            public string nodeIdCounter;
            public string time;
            public string description;
            public string id;
            public string label;

        }

        public class Metadata
        {

            public string id;
            public long time;
            public string reviewer;
            public string contentProvider;
            public string status;
            public Review review;
            public Ontology ontology;
            public Node node;
            public string description;
            public string label;
            public string colour;
            public int nodeIdCounter;
            public List<Cluster> cluster;

        }

        public class Cluster
        {
            public string id;
            public string ontologyId;
            public string label;
            public float entropy;
        }

        public class Review
        {
            public string objectives;
            public string limitations;
            public string variables;
            public string dataCollection1;
            public string rationale;
            public string sampling;
            public string hypothesis;
            public string levelDescription;
            public string title;
            public string dataCollection2;
        }




        void Label1Click(object sender, EventArgs e)
        {

        }

        void TextBox1TextChanged(object sender, EventArgs e)
        {
            this.answerList[4] = textBox1.Text;
        }

        void Label2Click(object sender, EventArgs e)
        {

        }

        void Label3Click(object sender, EventArgs e)
        {

        }

        void TextBox3TextChanged(object sender, EventArgs e)
        {
            this.answerList[6] = textBox3.Text;
        }

        void TextBox4TextChanged(object sender, EventArgs e)
        {
            this.answerList[8] = textBox4.Text;
        }

        void Form2Load(object sender, EventArgs e)
        {

        }

        void Label9Click(object sender, EventArgs e)
        {

        }

        void TextBox11TextChanged(object sender, EventArgs e)
        {

        }

        void ListView1SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        void Button3Click(object sender, EventArgs e)
        {

            var n = this.listBox1.Items.Count - 1;
            Node deletedNode = getNodeByLabel(this.listBox1.Items[n].ToString());
            Node lastNodeinList = getNodeByLabel(this.listBox1.Items[n - 1].ToString());
            upwardClosure.Remove(deletedNode);
            this.listBox1.Items.RemoveAt(n);

            this.comboBox2.SelectedIndex = -1;
            this.comboBox2.ResetText();
            this.comboBox2.Items.Clear();

            this.label3.Text = "Select children node of " + lastNodeinList.label;
            List<Node> childrenNodes = fetchChildrenNode(lastNodeinList);
            foreach (Node node in childrenNodes)
            {
                this.comboBox2.Items.Add(node.label);

            }
            if (n == 1)
            {
                this.button3.Visible = false;
            }
            // this.tableLayoutPanel2.Visible = false;
            // this.tableLayoutPanel2.Controls.Clear();

            //RemoveRows();


        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        void Label4Click(object sender, EventArgs e)
        {

        }

        void TextBox2TextChanged(object sender, EventArgs e)
        {
            this.answerList[5] = textBox2.Text;
            int occupantLoad = Int32.Parse(this.answerList[5]);
            if (occupantLoad >= 1 && occupantLoad <= 500)
            {
                this.answerList[7] = "2";
                comboBox5.Text = "2";
            }
            else if (occupantLoad >= 501 && occupantLoad <= 1000)
            {
                this.answerList[7] = "3";
                comboBox5.Text = "3";
            }
            else
            {
                this.answerList[7] = "4";
                comboBox5.Text = "4";
            }

            setEgressAndWidthRequirementValue(occupantLoad);



        }

        void Button4Click(object sender, EventArgs e)
        {
            String settingsValue = "[";
            for (int i = 0; i < 9; i++)
            {
                settingsValue = settingsValue + createSettingsIndividualRequest(i) + ",";
            }
            settingsValue = settingsValue.Remove(settingsValue.Length - 1, 1);
            settingsValue = settingsValue + "]";

            String settings = "\"settings\":" + settingsValue;
            //create a function to create question and answer string
            //join the strings
            //change the button 3 click function to make panel visible instead of making the table visible
            //make the table visible    	
            String upClosureString = "[";
            foreach (Node node in upwardClosure)
            {
                upClosureString += "\"" + node.id + "\"" + ",";

            }
            upClosureString = upClosureString.Remove(upClosureString.Length - 1, 1);
            upClosureString += "]";



            //TODO: fix getmetadata request by appending upward closure

            String url = String.Format("https://us-central1-sbir-80f98.cloudfunctions.net/revit/getMetaData");
            String payload = "{\"password\":\"uj287h7dnd342ijf\",\"ontologyId\":\"" + selectedOntology.id + "\",\"upwardsClosure\":" + upClosureString + ",";
            payload = payload + settings + "}";

            String response = callClientAPI(url, payload);
            //TaskDialog.Show("whoa", response);


            var serializer = new JavaScriptSerializer();
            var deserializedResult = serializer.Deserialize<List<Metadata>>(response);
            metadataList = deserializedResult.ToList();

            //begin sumit


            //end

            /*
            this.tableLayoutPanel2.Visible = false;
            this.tableLayoutPanel2.Controls.Clear();
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label16, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.label15, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label14, 1, 0);
            */





            //int columnCount = 0; 





            int panHeight = 10;
            foreach (Metadata metaData in metadataList)
            {




                //var panelChild = new System.Windows.Forms.Panel();

                var panelChild = new Panel();
                panelChild.Location = new Point(10, panHeight);
                panHeight = panHeight + 250;
                //panelChild.Size = new Size(1000,250);
                panelChild.AutoSize = true;
                this.panel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
                panelChild.BorderStyle = System.Windows.Forms.BorderStyle.None;

                //Label lbl = new Label();
                System.Windows.Forms.TextBox titleTextbox = new System.Windows.Forms.TextBox();
                titleTextbox.Text = metaData.label;
                titleTextbox.AutoSize = true;
                titleTextbox.ReadOnly = true;
                //titleTextbox.Multiline=true;
                titleTextbox.Location = new System.Drawing.Point(0, 0);
                titleTextbox.Width = 553;
                titleTextbox.Font = new Font(titleTextbox.Font.Name, 11, FontStyle.Bold);
                titleTextbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
                panelChild.Controls.Add(titleTextbox);

                System.Windows.Forms.TextBox descTextbox = new System.Windows.Forms.TextBox();
                descTextbox.Text = metaData.description;
                descTextbox.AutoSize = true;
                //descTextbox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
                descTextbox.ReadOnly = true;
                descTextbox.Multiline = true;
                descTextbox.Location = new System.Drawing.Point(0, 20);
                descTextbox.Size = new System.Drawing.Size(553, 77);
                descTextbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
                panelChild.Controls.Add(descTextbox);

                var clusterList = metaData.cluster;
                var tableLayout = new System.Windows.Forms.TableLayoutPanel();
                tableLayout.AutoSize = true;
                //tableLayout.AllowDrop = true;
                tableLayout.ColumnCount = 2;
                //tableLayout.Dock = System.Windows.Forms.DockStyle.Bottom;

                //tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.AutoSize));
                tableLayout.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
                tableLayout.Location = new System.Drawing.Point(0, 109);



                int dataNumber = 1;
                int rowCount = 1;


                Label headerEntropy = new Label();
                headerEntropy.Text = "Entropy";
                headerEntropy.AutoSize = true;
                //tableLayout.Controls.Add(label19, 0, 0);
                tableLayout.Controls.Add(headerEntropy, 1, 0);
                headerEntropy.Font = new Font(headerEntropy.Font.Name, 11, FontStyle.Bold);
                Label headerLabel = new Label();
                headerLabel.Text = "Label";
                headerLabel.AutoSize = true;
                headerLabel.Font = new Font(headerLabel.Font.Name, 11, FontStyle.Bold);
                //tableLayout.Controls.Add(label18, 0, 1);
                tableLayout.Controls.Add(headerLabel, 0, 0);

                foreach (Cluster clust in clusterList)
                {


                    Label temp = getLabelFromText(clust.label);
                    tableLayout.Controls.Add(temp, 0, rowCount);
                    temp = getLabelFromText(clust.entropy.ToString());
                    tableLayout.Controls.Add(temp, 1, rowCount);
                    rowCount += 1;
                }





                panelChild.Controls.Add(tableLayout);

                this.panel2.Controls.Add(panelChild);

                panelChild.Visible = true;
                tableLayout.Visible = true;




                dataNumber++;
                rowCount++;
                //columnCount++;




            }



            //this.tableLayoutPanel2.Visible = true;
            //this.tableLayoutPanel2.RowCount = rowCount;
            //this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.AutoSize));



        }

        String createSettingsIndividualRequest(int i)
        {
            String result = "{" + "\"question\":\"" + (i + 1).ToString() + "\",\"answer\":\"" + answerList[i] + "\"}";
            return result;

        }
        void RadioButton2CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                this.answerList[0] = "No";
            }
        }

        void RadioButton1CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                this.answerList[0] = "Yes";
            }
        }

        void ComboBox3SelectedIndexChanged(object sender, EventArgs e)
        {
            this.answerList[1] = comboBox3.Text;
        }

        void CheckedListBox1SelectedIndexChanged(object sender, EventArgs e)
        {
            this.answerList[2] = "";
            foreach (object item in checkedListBox1.CheckedItems)
            {
                this.answerList[2] = this.answerList[2] + item.ToString();
            }
        }

        void ComboBox4SelectedIndexChanged(object sender, EventArgs e)
        {
            this.answerList[3] = comboBox4.Text;
            if (this.answerList[3].Contains('H') || this.answerList[3] == "I-2")
            {
                occupantLoadMultiplier = 0.2;
            }
            else
                occupantLoadMultiplier = 0.3;
        }

        void ComboBox5SelectedIndexChanged(object sender, EventArgs e)
        {
            this.answerList[7] = comboBox5.Text;
        }

        void Panel2Paint(object sender, PaintEventArgs e)
        {

        }

        void TableLayoutPanel2Paint(object sender, PaintEventArgs e)
        {

        }

        void setEgressAndWidthRequirementValue(int occupantLoad)
        {
            double egressValue = occupantLoadMultiplier * occupantLoad;
            this.answerList[6] = egressValue.ToString();
            textBox3.Text = egressValue.ToString();

            if (egressValue > 44)
            {

                this.answerList[8] = egressValue.ToString();
                textBox4.Text = egressValue.ToString();
            }
            else
            {
                this.answerList[8] = "44";
                textBox4.Text = "44";
            }

        }
    }

}
