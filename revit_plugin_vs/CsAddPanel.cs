﻿using System;
using System.Reflection;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System.Windows.Media.Imaging;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI.Selection;
using System.Collections.Generic;

namespace Revit_Plugin
{
    /// <remarks>
    /// This application's main class. The class must be Public.
    /// </remarks>
    public class CsAddPanel : IExternalApplication
    {
        // Both OnStartup and OnShutdown must be implemented as public method
        public Result OnStartup(UIControlledApplication application)
        {

            // Create a custom ribbon tab
            String tabName = "CrossWalk";
            application.CreateRibbonTab(tabName);
            // Add a new ribbon panel
            RibbonPanel ribbonPanel = application.CreateRibbonPanel(tabName, "Tools");

            // Create a push button to trigger a command add it to the ribbon panel.
            string thisAssemblyPath = Assembly.GetExecutingAssembly().Location;
            PushButtonData buttonData = new PushButtonData("cmdRevit_Plugin",
               "Run API", thisAssemblyPath, "Revit_Plugin.Document_Selection");

            PushButton pushButton = ribbonPanel.AddItem(buttonData) as PushButton;

            // Optionally, other properties may be assigned to the button
            // a) tool-tip
            pushButton.ToolTip = "Click to connect to API";
           
            // b) large bitmap
            Uri uriImage = new Uri(@"Images\favicon.ico", UriKind.Relative);
         BitmapImage largeImage = new BitmapImage(uriImage);
         pushButton.LargeImage = largeImage;

         return Result.Succeeded;
      }

      public Result OnShutdown(UIControlledApplication application)
      {
         // nothing to clean up in this simple case
         return Result.Succeeded;
      }
   }
   /// <remarks>
   /// The "Revit_plugin" external command. The class must be Public.
   /// </remarks>
   [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    public class Revit_Plugin : IExternalCommand
        {
            // The main Execute method (inherited from IExternalCommand) must be public
            public Autodesk.Revit.UI.Result Execute(ExternalCommandData revit,
                ref string message, ElementSet elements)
            {
                TaskDialog.Show("Revit", "Ontology");
                return Autodesk.Revit.UI.Result.Succeeded;
            }
        }
    [Autodesk.Revit.Attributes.Transaction(TransactionMode.ReadOnly)]
    public class Document_Selection : IExternalCommand
    {
        public Autodesk.Revit.UI.Result Execute(ExternalCommandData commandData,
                ref string message, ElementSet elements)
        {
            try
            {
                // Select some elements in Revit before invoking this command
                using (var form = new Form2())
                {
                    //use ShowDialog to show the form as a modal dialog box.

                    form.ShowDialog();
                   // selectedOntology = form.selectedItem;

                }
              
            }
            catch (Exception e)
            {
                message = e.Message;
                return Autodesk.Revit.UI.Result.Failed;
            }

            return Autodesk.Revit.UI.Result.Succeeded;
        }
    }
}

